<?php

return [
    "tashkilot"=>"\"Xorazm hududiy elektr tarmoqlari\" AJ",
    "ish-vaqti"=>"Du - Jum 09:00 - 18:00",
    "phone"=>"Telefon",
    "tel"=>"227-26-75",
    "epochta"=>"xorazm-elektr@umail.uz",
    "manzil"=>"Xorazm viloyati,220100 Urganch sh., Xonqa ko'chasi 36/1 uy.",
    "manzil-qisqa"=>"Urganch sh., Xonqa k. 36/1 uy",
    "address"=>"Manzil",
    "izlash"=>"Izlash",
    "boglanish"=>"Bog'lanish",
    "Bizning-telefon"=>"Bizning telefon",
    "Bizning-email"=>"Bizning email",
    "Bizning-ish-vaqtimiz"=>"Bizning ish vaqtimiz",
    "footer-txt"=>"2014 yil 23 iyuldan tashkiliy huquqiy shakliga koʼra «Xorazm hududiy elektr tarmoklari» aktsiyadorlik jamiyati sifatida oʼz faoliyatini olib bormoqda.",
    "yangiliklar"=>"Yangiliklar",
    "barcha-yangiliklar"=>"Barcha yangiliklar",
    "subscribe-txt"=>"So'ngi yangiliklarga email orqali obuna bo'ling.",
    "yuborish"=>"Yuborish",
    "kontaklar"=>"Kantaktlar",
    "kontak"=>"Bog'lanish",
    "Bosh-sahifa"=>"Bosh sahifa",
    "Kantaktlarimiz"=>"Kantaktlarimiz",
    "Bizga-xabar-yollang"=>"Bizga xabar yo'llang!",
    "Bizga-xabar-yollang-txt"=>"Quyidagi formani to'ldiring va bizga habar yo'llang. Bizning mutaxasislar tez orada siz bilan bog'lanishadi.",
    "ism"=>"Ism",
    "Xabar-matni"=>"Xabar matni",
    "Xabar-muvofaqqiyatli-yuborildi"=>"Xabar muvofaqqiyatli yuborildi!",
    "Xabar"=>"Xabar",
    "Xorazm-viloyat"=>"Xorazm viloyat",
    "hududiyelektrtarmoqlari"=>"<strong>hududiy</strong> elektr tarmoqlari",
    "Yillardavomidasizningxizmatingizda"=>"Yillar davomida sizning xizmatingizda",
    "Bizbilanboglanish"=>"Biz bilan bog'lanish",
    "Xorazmhududiyelektrtarmoqlari"=>"Xorazm hududiy elektr tarmoqlari",
    "Xorazmhududiyelektrtarmoqlari-txt"=>"Oʼzbekiston Respublikasi Vazirlar Maxkamasining 26 noyabr 1999 yil № 511-sonli, Oʼzbekiston Respublikasi Vazirlar Maxkamasining 9 mart 2001 yil №119-sonli, Xorazm elektr tarmoklari shoʼba korxonasi Davlat mulk kumitasi Xorazm viloyat hududiy boshkarmasining 2001 yil 23 iyundagi № 794-X sonli buyrug'iga asosan «Xorazm elektr tarmoklari» shoʼba korxonasiga aylantirildi va 2014 yil 23 iyuldan tashkiliy huquqiy shakliga koʼra «Xorazm hududiy elektr tarmoklari» aktsiyadorlik jamiyati sifatida oʼz faoliyatini olib bormoqda.",
    "KALKUYLATOR"=>"KALKUYLATOR",
    "Elektrenergiyasinihisoblabkoring"=>"Elektr energiyasini hisoblab ko'ring",
    "Hisoblashnatijasi"=>"Hisoblash natijasi",
    "2001-txt"=>"2001 yildan buyun sizga o'z xizmatlarini ko'rsatib kelmoqda",
    "Batafsil"=>"Batafsil",
    "2014-txt"=>"2014 yil 23 iyuldan tashkiliy huquqiy shakliga koʼra «Xorazm hududiy elektr tarmoklari» aktsiyadorlik jamiyati sifatida oʼz faoliyatini olib bormoqda.",
    "comments"=>"Izohlar",
    "comment"=>"Izoh",
    "add-comment"=>"Izoh qoldirish",
    "close"=>"Yopish",
    "send"=>"Yuborish",
    "Havolalar"=>"Havolalar",
    "Hisoblash"=>"Hisoblash",
    "guruh"=>"guruh",
    "Oddiyuy"=>"Oddiy uy",
    "Elektrplitaliuy"=>"Elektr plitali uy",
    "kvts"=>"kVt·s",
    "Itemolchi"=>"Iste'molchi",
    "Turi"=>"Turi",
    "Miqdor"=>"Miqdor",
    "Miqdorturi"=>"Miqdor turi",
    "Maishiyistemolchi"=>"Maishiy iste'molchi",
    "Yuridikistemolchi"=>"Yuridik iste'molchi",
    "yangiliklar-topilmadi"=>"Yangiliklar topilmadi",
    "1"=>"Bilimdan qudratliroq kuch yo‘q, bilim bilan qurollangan odam yengilmasdir.",
    "2"=>" Ma’naviy hayotda ham amaliy hayotdagidek, kimki bilimga tayansagina to‘xtovsiz kamol topadi va yutuqlarga erishaveradi.",
    "3"=>"Maktab hayoti",
    "4"=>"Narsalar qanday bo‘lmog‘i lozimligini bilish aqlli odamga xosdir; narsalarning haqiqatda qandayligini bilish tajribali odamga xos; narsalarni yanada takomillashtirishni bi-lish buyuk odamga xos.",
    "5"=>"Elektron kutubxona",
    "6"=>"Xalqaro Tarmoqdagi minglab kitoblarni o'z ichiga oladigan saytlar elektron kutubxonami yoki kutubxona saytlari hammi? Raqamli kolleksiyalarni yaratish uchun qanday bilim va malakalar kerak bo'ladi? Ushbu ma`ruzada shu kabi savollarga javob topishga harakat qilamiz (maqolada \"elektron kutubxona\", \"e-kutubxona\" va \"raqamli kutubxona\" terminlari bir ma'noda qo'llaniladi.",
    "7"=>"Savol javob",
    "8"=>"Matkab haqidagi o'zingizni qiziqtirgan savollarga javob oling!<br> Sizni qiziqtirgan savol javobini topa olmadingizmi unda bizga xabar yo'llang.",
    "9"=>"`Bu yerda maktab hayotidagi so'nggi yangiliklar`",
    "10"=>"Bepul kurslar<br>Yoz davomida!",
    "11"=>"O’qish, o‘rganishda mardi maydon bo‘l! Sening qadr-qimmating — o‘z imkoniyatlaringdan to‘la foydalanib o‘qishingda. Er kishining izzat, sha’ni — tekinxo‘r, hamtovoq bo‘lmaslikda. Fikr yalqovligidan hazar qil!",
    "12"=>"Tajribali o'qituvchilar",
    "13"=>"Butun yoz davomida bizning tajribali o'qituvchilar bilan bepul kurslar tashkil qilinadi.",
    "14"=>"Sertifikatlar",
    "15"=>"Kurslarni a'loga tugatgan o'quvchilar maktabimiz tomonidan beriladigan sertificatlarga ega bo'ladilar.",
    "16"=>"O'zing qiziqqan kursni tanla",
    "17"=>"Kursni tanlag",
    "18"=>"Maktabimiz o'qituvchlari",
    "19"=>"inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach.inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach.",
    "20"=>"Bizga savolingiz bormi?",
    "21"=>"Sahifa topilmadi",
    "22"=>"Kechirasiz sahifa topilmadi",
    "23"=>"Bosh sahifaga qaytish",
    "24"=>"Xizmatlat litsenziyalangan ",
    "25"=>"Saytni yaratuvchisi",
    "26"=>"Xizmatlar",
    "27"=>"Bo'sh ish o'rinlari",
    "28"=>"",
    "29"=>"",
    "30"=>"",
    "31"=>"",
    "32"=>"",
    "33"=>"",
    "34"=>"",
    "35"=>"",
    "36"=>"",
    "37"=>"",
    "38"=>"",
    "39"=>"",
    "40"=>"",
    "41"=>"",
    "42"=>"",
    "43"=>"",
    "44"=>"",
    "45"=>"",
    "46"=>"",
    "47"=>"",
    "48"=>"",
    "49"=>"",
    "50"=>"",
];
