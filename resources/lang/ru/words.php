<?php

return [
    "tashkilot"=>"\"Хорезмские региональные электрические сети\" АО",
    "ish-vaqti"=>"Пн - Пт 09:00 - 18:00",
    "phone"=>"Телефон",
    "tel"=>"227-26-75",
    "epochta"=>"xorazm-elektr@umail.uz",
    "manzil"=>"Хорезмская область, 220100 г. Ургенч, Улица Хонка, дом 36/1.",
    "manzil-qisqa"=>"Г. Ургенч, Хонка у. Дом 36/1",
    "address"=>"Адрес",
    "izlash"=>"Izlash",
    "boglanish"=>"Связаться с нами",
    "Bizning-telefon"=>"Наш телефон",
    "Bizning-email"=>"Наш email",
    "Bizning-ish-vaqtimiz"=>"Наше рабочее время",
    "footer-txt"=>"С 23 июля 2014 года по организационно-правовой форме осуществляет свою деятельность как акционерное общество «Хорезмские территориальные электрические сети».",
    "yangiliklar"=>"Новости",
    "barcha-yangiliklar"=>"Все новости",
    "subscribe-txt"=>"Подпишитесь на последние новости по электронной почте.",
    "yuborish"=>"Отправить",
    "kontaklar"=>"Кантакты",
    "kontak"=>"Контакт",
    "Bosh-sahifa"=>"Пустая страница",
    "Kantaktlarimiz"=>"Наши контакты",
    "Bizga-xabar-yollang"=>"Отправьте нам сообщение!",
    "Bizga-xabar-yollang-txt"=>"Заполните форму ниже и отправьте нам сообщение. Наши специалисты скоро свяжутся с вами.",
    "ism"=>"Имя",
    "Xabar-matni"=>"Текст сообщения",
    "Xabar-muvofaqqiyatli-yuborildi"=>"Сообщение отправлено успешно!",
    "Xabar"=>"Сообщение",
    "Xorazm-viloyat"=>"Хорезмская область",
    "hududiyelektrtarmoqlari"=>"<strong>территориальные </strong>  электрические сети",
    "Yillardavomidasizningxizmatingizda"=>"На протяжении многих лет в вашем обслуживании.",
    "Bizbilanboglanish"=>"Связаться с нами",
    "Xorazmhududiyelektrtarmoqlari"=>"Хорезмские региональные электрические сети",
    "Xorazmhududiyelektrtarmoqlari-txt"=>"В соответствии с распоряжением Кабинета Министров Республики Узбекистан от 26 ноября 1999 года № 511, Кабинета Министров Республики Узбекистан от 9 марта 2001 года №119, Хорезмского филиала Государственного комитета по собственности Хорезмской области от 23 июня 2001 года № 794-х территориальное управление преобразовано в дочернее предприятие «хорезмские электрические сети» и с 23 июля 2014 года по организационно-правовой форме осуществляет свою деятельность в акционерное общество «хорезмские территориальные электрические сети». ведет.",
    "KALKUYLATOR"=>"КАЛЬКУЙЛАТОР",
    "Elektrenergiyasinihisoblabkoring"=>"Попробуйте рассчитать электрическую энергию",
    "Hisoblashnatijasi"=>"Результат расчета",
    "2001-txt"=>"С 2001 года оказывает вам свои услуги",
    "Batafsil"=>"Подробнее",
    "2014-txt"=>"С 23 июля 2014 года по организационно-правовой форме осуществляет свою деятельность как акционерное общество «хорезмские территориальные электрические сети».",
    "comments"=>"Комментарии",
    "comment"=>"Комментарий",
    "add-comment"=>"Оставить комментарий",
    "close"=>"Закрыть",
    "send"=>"Отправить",
    "Havolalar"=>"Ссылки",
    "Hisoblash"=>"Расчет",
    "guruh"=>"группа",
    "Oddiyuy"=>"Простой дом",
    "Elektrplitaliuy"=>"Дом с электроплитой",
    "kvts"=>"кВт·ч",
    "Itemolchi"=>"Потребитель",
    "Turi"=>"Тип",
    "Miqdor"=>"Количество",
    "Miqdorturi"=>"Тип количество",
    "Maishiyistemolchi"=>"Бытовой потребитель",
    "Yuridikistemolchi"=>"Юридический потребитель",
    "yangiliklar-topilmadi"=>"Новостей не найдено",
    "1"=>"Вне знания нет силы, и человек, вооруженный знанием, не может быть побежден.",
    "2"=>"В духовной жизни, как и в практической жизни, только те, кто опирается на знания, будут расти и достигать успеха.",
    "3"=>"Школьная жизнь",
    "4"=>"Для человека разумно знать, как все должно идти; знание того, как обстоят дела на самом деле, типично для опытного человека; знать, как сделать вещи лучше, принадлежит великому человеку.",
    "5"=>"Электронная библиотека",
    "6"=>"Сайты, которые содержат тысячи книг в Международной сети, являются электронными библиотеками или библиотечными сайтами? Какие знания и навыки необходимы для создания цифровых коллекций? В этом отчете мы попытаемся найти ответы на такие вопросы (термины «электронная библиотека», «электронная библиотека» и «цифровая библиотека» используются в одном и том же смысле).",
    "7"=>"Ответь на вопрос",
    "8"=>"Получите ответы на свои вопросы о тренировке!<br>Если вы не нашли ответ, который вас интересует, сообщите нам об этом.",
    "9"=>"`Вот последние новости в школьной жизни`",
    "10"=>"Бесплатные курсы<br>Летом!",
    "11"=>"Читайте дальше, веселиться! Ваше достоинство - в полной мере использовать ваши возможности. Честь и достоинство человека свободны, неуправляемы. Остерегайтесь лени идеи!",
    "12"=>"Опытные учителя",
    "13"=>"Бесплатные курсы организуются с нашими опытными преподавателями в течение всего лета.",
    "14"=>"Сертификаты",
    "15"=>"Студенты, окончившие курсы, получат сертификаты, выданные нашей школой.",
    "16"=>"Выберите интересующий вас курс",
    "17"=>"Выберите курс",
    "18"=>"Учителя нашей школы",
    "19"=>"inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach.inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach.",
    "20"=>"У вас есть вопросы?",
    "21"=>"Страница не найдена",
    "22"=>"Извините, страница не найдена",
    "23"=>"Вернуться на главную страницу",
    "24"=>"Сервис лицензирован",
    "25"=>"Разработано",
    "26"=>"Услуги",
    "27"=>"Вакансии",


];
