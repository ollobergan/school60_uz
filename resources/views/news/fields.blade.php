<!-- Title Uz Field -->
<div class="form-group col-sm-12">
    {!! Form::label('title_uz', 'Sarlovha Uz:') !!}
    {!! Form::text('title_uz', null, ['class' => 'form-control']) !!}
</div>

<!-- Title Ru Field -->
<div class="form-group col-sm-12">
    {!! Form::label('title_ru', 'Sarlovha Ru:') !!}
    {!! Form::text('title_ru', null, ['class' => 'form-control']) !!}
</div>

<!-- Title En Field -->
<div class="form-group col-sm-12">
    {!! Form::label('title_en', 'Sarlovha En:') !!}
    {!! Form::text('title_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Short Uz Field -->
<div class="form-group col-sm-12">
    {!! Form::label('short_uz', 'Qisqacha matn Uz:') !!}
    {!! Form::textarea('short_uz', null, ['class' => 'form-control']) !!}
</div>

<!-- Short Ru Field -->
<div class="form-group col-sm-12">
    {!! Form::label('short_ru', 'Qisqacha matn Ru:') !!}
    {!! Form::textarea('short_ru', null, ['class' => 'form-control']) !!}
</div>

<!-- Short En Field -->
<div class="form-group col-sm-12">
    {!! Form::label('short_en', 'Qisqacha matn En:') !!}
    {!! Form::textarea('short_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Content Uz Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content_uz', 'Batafsil Uz:') !!}
    {!! Form::textarea('content_uz', null, ['class' => 'form-control']) !!}
</div>

<!-- Content Ru Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content_ru', 'Batafsil Ru:') !!}
    {!! Form::textarea('content_ru', null, ['class' => 'form-control']) !!}
</div>

<!-- Content En Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content_en', 'Batafsil En:') !!}
    {!! Form::textarea('content_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-12">
    {!! Form::label('image', 'Rasm:') !!}
    {!! Form::text('image', null, ['class' => 'form-control','readonly','id'=>'fieldID2']) !!}
    <a href="{{url('/')}}/filemanager/dialog.php?type=2&amp;field_id=fieldID2&amp;relative_url=1" class="btn btn-outline-primary iframe-btn" type="button">Rasm biriktirish</a>
</div>
<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Saqlash', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('news.index') }}" class="btn btn-danger">Bekor qilish</a>
</div>
