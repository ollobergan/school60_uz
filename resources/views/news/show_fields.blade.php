<table class="table table-bordered">
    <tbody>
    <tr>
        <td>
            {!! Form::label('id', 'ID:') !!}
            <p>{{ $news->id }}</p>
            {!! Form::label('created_at', 'Yaratilgan vaqti:') !!}
            <p>{{ $news->created_at }}</p>
        </td>
        <td colspan="2">
            <img src="{{url('/source/'.$news->image)}}" width="200">
        </td>

    </tr>
    <tr>
        <td><div class="form-group">
                {!! Form::label('title_uz', 'Sarlovha Uz:') !!}
                <p>{{ $news->title_uz }}</p>
            </div>
        </td>
        <td>
            <div class="form-group">
                {!! Form::label('title_ru', 'Sarlovha Ru:') !!}
                <p>{{ $news->title_ru }}</p>
            </div>
        </td>
        <td>
            <div class="form-group">
                {!! Form::label('title_en', 'Sarlovha En:') !!}
                <p>{{ $news->title_en }}</p>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">
                {!! Form::label('short_uz', 'Qisqacha matn Uz:') !!}
                <p>{!! $news->short_uz !!}</p>
            </div>
        </td>
        <td>
            <div class="form-group">
                {!! Form::label('short_ru', 'Qisqacha matn Ru:') !!}
                <p>{!!  $news->short_ru !!}</p>
            </div>
        </td>
        <td>
            <div class="form-group">
                {!! Form::label('short_en', 'Qisqacha matn En:') !!}
                <p>{!! $news->short_en !!}</p>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">
                {!! Form::label('content_uz', 'Batafsil Uz:') !!}
                <p>{!!  $news->content_uz !!}</p>
            </div>
        </td>
        <td>
            <div class="form-group">
                {!! Form::label('content_ru', 'Batafsil Ru:') !!}
                <p>{!! $news->content_ru !!}</p>
            </div>
        </td>
        <td>
            <div class="form-group">
                {!! Form::label('content_en', 'Batafsil En:') !!}
                <p>{!! $news->content_en !!}</p>
            </div>
        </td>
    </tr>
    </tbody>
</table>


<table class="table table-bordered">
    <thead>
    <tr>
        <td>Komentariyalar</td>
        <td>O'chirish</td>
    </tr>
    </thead>
    <tbody>
    @foreach($comments as $comment)
        <tr>
            <td>
                <b>{{$comment->name}}</b> <span>{{date('d-m-Y H:i',strtotime($comment->created_at))}}</span><br>
                {!! $comment->message !!}
            </td>
            <td>
                {!! Form::open(['route' => ['comments.destroy', $comment->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    {!! Form::button('<i class="fa fa-trash"></i>', [
                        'type' => 'submit',
                        'class' => 'btn btn-outline-danger',
                        'onclick' => "return confirm('Are you sure?')"
                    ]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<!-- Created At Field -->



