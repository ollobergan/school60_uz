@extends('layouts.frontend')

@section('content')
    <?php
    $lang_title = "title_".App::getLocale();
    $lang_content = "content_".App::getLocale();
    ?>


    <section class="banner-area relative" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                        {{$post->$lang_title}}
                    </h1>
                    <p class="text-white link-nav">
                        <a href="{{url('/')}}">{{__('words.Bosh-sahifa')}} </a>
                        <span class="lnr lnr-arrow-right"></span>
                        <a href="{{url('/news')}}">{{__('words.yangiliklar')}} </a> <span class="lnr lnr-arrow-right"></span>

                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="event-details-area section-gap">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 posts-list">
                    <div class="single-post row">
                        <div class="col-lg-12 text-center">
                            <div class="feature-img">
                                <img class="img-fluid" src="{{url('/source/'.$post->image)}}" alt="">
                            </div>
                        </div>
                        <div>
                            <h3 class="mt-20 mb-20">{{$post->$lang_title}}</h3>
                            <span><i class="fa fa-calendar"></i> {{date('d.m.y',strtotime($post->created_at))}}</span>
                            <span><i class="fa fa-eye"></i> {{$post->view_cnt}}</span>
                            <span><i class="fa fa-comment"></i> {!! $post->comment_cnt !!}</span>
                            <hr>
                            {!! $post->$lang_content !!}
                        </div>
                    </div>
                    <div id="comments"></div>
                </div>
            </div>
        </div>
    </section>



@endsection
@section('scripts')

    <script type="text/javascript">
        comments();
        function comments(){
            $.ajax({
                url : "{{url('/get-news-comments/'.$post->id)}}",
                method:"get",
                success(data){
                    $("#comments").html(data);
                },
            });
        }
    </script>
@endsection
