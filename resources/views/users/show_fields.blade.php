<table class="table table-bordered">
    <tr>
        <td>{!! Form::label('id', 'Id:') !!}</td>
        <td>{{ $user->id }}</td>
    </tr>
    <tr>
        <td>{!! Form::label('name', 'Ismi:') !!}</td>
        <td>{{ $user->name }}</td>
    </tr>
    <tr>
        <td>{!! Form::label('email', 'Email:') !!}</td>
        <td>{{ $user->email }}</td>
    </tr>
    <tr>
        <td>{!! Form::label('created_at', 'Qo\'shilgan vaqt:') !!}</td>
        <td>{{ $user->created_at }}</td>
    </tr>
</table>