
@extends('layouts.frontend')

@section('content')


    <!-- start banner Area -->
    <section class="banner-area relative about-banner" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                        Biz bilan bog'lanish
                    </h1>
                    <p class="text-white link-nav"><a href="index.html">Asosiy </a>  <span class="lnr lnr-arrow-right"></span>  <a href="contact.html">Biz bilan bog'lanish</a></p>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->

    <!-- Start contact-page Area -->
    <section class="contact-page-area section-gap">
        <div class="container">
            <div class="row">
                <div class="map-wrap" style="width:100%; height: 445px;" id="map"></div>
                <div class="col-lg-4 d-flex flex-column address-wrap">
                    <div class="single-contact-address d-flex flex-row">
                        <div class="icon">
                            <span class="lnr lnr-home"></span>
                        </div>
                        <div class="contact-details">
                            <h5>O'zbekiston, Qoraqalpog'iston Respublikasi</h5>
                            <p>
                                To`rtko`l tumani,Tozabog`yop OFY, Toshqal`a ko’cha 51-uy
                            </p>
                        </div>
                    </div>
                    <div class="single-contact-address d-flex flex-row">
                        <div class="icon">
                            <span class="lnr lnr-phone-handset"></span>
                        </div>
                        <div class="contact-details">
                            <h5>+99899–956–30–95</h5>
                            <p>Dushanba-Shanba, ish vaqti: 6:00-21:00</p>
                        </div>
                    </div>
                    <div class="single-contact-address d-flex flex-row">
                        <div class="icon">
                            <span class="lnr lnr-envelope"></span>
                        </div>
                        <div class="contact-details">
                            <h5><a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="4a393f3a3a25383e0a292526253826232864292527">info@shchool60.uz</a></h5>

                            <p>Savollaringgizni bizga yuboring</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <form class="form-area contact-form text-right" id="myForm" action="mail.php" method="post">
                        <div class="row">
                            <div class="col-lg-12 form-group">
                                <input name="name" placeholder="Ism" class="common-input mb-20 form-control" type="text" id="name">
                                <input name="email" placeholder="E-mail manzilingzni kiriting" class="common-input mb-20 form-control" id="email" type="email">
                                <textarea class="common-textarea form-control" name="message" placeholder="Xabarni Yozing" id="message"></textarea>
                            </div>
                            <div class="col-lg-12">
                                <button class="genric-btn primary" style="float: right;" onclick="send_message()"> {{__('words.yuborish')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- End contact-page Area -->






    <script>
        function send_message(){

            error = 0;
            if ($('#name').val()==''){
                error = 1;
                $('#name').addClass('has-error');
            } else{
                $('#name').removeClass('has-error');
            }
            if ($('#email').val()==''){
                error = 1;
                $('#email').addClass('has-error');
            } else{
                $('#email').removeClass('has-error');
            }
            if ($('#message').val()==''){
                error = 1;
                $('#message').addClass('has-error');
            } else{
                $('#message').removeClass('has-error');
            }
            if (error){
                return;
            }

            $.ajax({
                url : "{{url('/contact')}}",
                method:"post",
                data : {
                    "_token": $('#csrf-token')[0].content,
                    "name":$('#name').val(),
                    "email":$('#email').val(),
                    "message":$('#message').val(),
                },
                success(data){
                    $('#name').val('');
                    $('#email').val('');
                    $('#message').val('');
                    Swal.fire(
                        '{{__('words.Xabar')}}',
                        '{{__('words.Xabar-muvofaqqiyatli-yuborildi')}}!',
                        'success'
                    )
                },
            });
        }
    </script>
@endsection