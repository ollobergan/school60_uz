<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $page->id }}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'URL:') !!}
    <p>{{ $page->slug }}</p>
</div>

<!-- Title Uz Field -->
<div class="form-group">
    {!! Form::label('title_uz', 'Sarlovha Uz:') !!}
    <p>{{ $page->title_uz }}</p>
</div>

<!-- Title Ru Field -->
<div class="form-group">
    {!! Form::label('title_ru', 'Sarlovha Ru:') !!}
    <p>{{ $page->title_ru }}</p>
</div>

<!-- Title En Field -->
<div class="form-group">
    {!! Form::label('title_en', 'Sarlovha En:') !!}
    <p>{{ $page->title_en }}</p>
</div>

<!-- Content Uz Field -->
<div class="form-group">
    {!! Form::label('content_uz', 'Text Uz:') !!}
    <p>{{ $page->content_uz }}</p>
</div>

<!-- Content Ru Field -->
<div class="form-group">
    {!! Form::label('content_ru', 'Text Ru:') !!}
    <p>{{ $page->content_ru }}</p>
</div>

<!-- Content En Field -->
<div class="form-group">
    {!! Form::label('content_en', 'Text En:') !!}
    <p>{{ $page->content_en }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Qo\'shilgan vaqti:') !!}
    <p>{{ $page->created_at }}</p>
</div>

