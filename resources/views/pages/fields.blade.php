<!-- Slug Field -->
<div class="form-group col-sm-12">
    {!! Form::label('slug', 'Menyu:') !!}
    {!! Form::select('slug', $menus, null, ['class' => 'form-control']) !!}
</div>

<!-- Title Uz Field -->
<div class="form-group col-sm-12">
    {!! Form::label('title_uz', 'Sarlovha Uz:') !!}
    {!! Form::text('title_uz', null, ['class' => 'form-control']) !!}
</div>

<!-- Title Ru Field -->
<div class="form-group col-sm-12">
    {!! Form::label('title_ru', 'Sarlovha Ru:') !!}
    {!! Form::text('title_ru', null, ['class' => 'form-control']) !!}
</div>

<!-- Title En Field -->
<div class="form-group col-sm-12">
    {!! Form::label('title_en', 'Sarlovha En:') !!}
    {!! Form::text('title_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Content Uz Field -->
<div class="form-group col-sm-12">
    {!! Form::label('content_uz', 'Text Uz:') !!}
    {!! Form::textarea('content_uz', null, ['class' => 'form-control']) !!}
</div>

<!-- Content Ru Field -->
<div class="form-group col-sm-12">
    {!! Form::label('content_ru', 'Text Ru:') !!}
    {!! Form::textarea('content_ru', null, ['class' => 'form-control']) !!}
</div>

<!-- Content En Field -->
<div class="form-group col-sm-12">
    {!! Form::label('content_en', 'Text En:') !!}
    {!! Form::textarea('content_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Saqlash', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('pages.index') }}" class="btn btn-danger">Bekor qilish</a>
</div>
