@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('pages.index') !!}">Sahifalar</a>
          </li>
          <li class="breadcrumb-item active">O'zgartirish</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Sahifani o'zgartirish</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($page, ['route' => ['pages.update', $page->id], 'method' => 'patch']) !!}

                              @include('pages.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection