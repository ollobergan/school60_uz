@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item active">Boshqaruv paneli</li>
    </ol>
  <div class="container-fluid">
        <div class="animated fadeIn">
             <div class="row">
                 <div class="col-12 col-lg-6">
                     <div class="card">
                         <div class="card-body p-3 d-flex align-items-center">
                             <i class="fa fa-newspaper-o bg-primary p-3 font-2xl mr-3"></i>
                             <div>
                                 <div class="text-value-sm text-primary">{{$data->news_cnt}}</div>
                                 <div class="text-muted text-uppercase font-weight-bold small">Yangiliklar</div>
                             </div>
                         </div>
                     </div>
                 </div>

                 <div class="col-12 col-lg-6">
                     <div class="card">
                         <div class="card-body p-3 d-flex align-items-center">
                             <i class="fa fa-book bg-primary p-3 font-2xl mr-3"></i>
                             <div>
                                 <div class="text-value-sm text-primary">{{$data->pages_cnt}}</div>
                                 <div class="text-muted text-uppercase font-weight-bold small">Sahifalar</div>
                             </div>
                         </div>
                     </div>
                 </div>

                 <div class="col-12 col-lg-6">
                     <div class="card">
                         <div class="card-body p-3 d-flex align-items-center">
                             <i class="fa fa-envelope bg-primary p-3 font-2xl mr-3"></i>
                             <div>
                                 <div class="text-value-sm text-primary">{{$data->messages_cnt}}</div>
                                 <div class="text-muted text-uppercase font-weight-bold small">Xabarlar</div>
                             </div>
                         </div>
                     </div>
                 </div>

                 <div class="col-12 col-lg-6">
                     <div class="card">
                         <div class="card-body p-3 d-flex align-items-center">
                             <i class="fa fa-user-secret bg-primary p-3 font-2xl mr-3"></i>
                             <div>
                                 <div class="text-value-sm text-primary">{{$data->admin_cnt}}</div>
                                 <div class="text-muted text-uppercase font-weight-bold small">Administratorlar</div>
                             </div>
                         </div>
                     </div>
                 </div>
            </div>
        </div>
    </div>
</div>
@endsection
