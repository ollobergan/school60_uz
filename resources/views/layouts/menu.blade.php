
<li class="nav-item {{ Request::is('news*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('news.index') }}">
        <i class="nav-icon fa fa-newspaper-o"></i>
        <span>Yangiliklar</span>
    </a>
</li>
<li class="nav-item {{ Request::is('contactMessages*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('contactMessages.index') }}">
        <i class="nav-icon fa fa-envelope"></i>
        <span>Xabarlar</span>
    </a>
</li>
<li class="nav-item {{ Request::is('pages*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('pages.index') }}">
        <i class="nav-icon fa fa-book"></i>
        <span>Sahifalar</span>
    </a>
</li>
{{--<li class="nav-item {{ Request::is('employees*') ? 'active' : '' }}">--}}
    {{--<a class="nav-link" href="{{ route('employees.index') }}">--}}
        {{--<i class="nav-icon fa fa-users"></i>--}}
        {{--<span>Hodimlar</span>--}}
    {{--</a>--}}
{{--</li>--}}
<li class="nav-item {{ Request::is('comments*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('comments.index') }}">
        <i class="nav-icon fa fa-comment"></i>
        <span>Komentariyalar</span>
    </a>
</li>
<li class="nav-item {{ Request::is('menus*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('menus.index') }}">
        <i class="nav-icon fa fa-navicon"></i>
        <span>Menyular</span>
    </a>
</li>
<li class="nav-item {{ Request::is('users*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('users.index') }}">
        <i class="nav-icon fa fa-user-secret"></i>
        <span>Administratorlar</span>
    </a>
</li>
<li class="nav-item {{ Request::is('courses*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('courses.index') }}">
        <i class="nav-icon fa fa-star"></i>
        <span>Kurslar</span>
    </a>
</li>
<li class="nav-item {{ Request::is('courseMessages*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('courseMessages.index') }}">
        <i class="nav-icon fa fa-envelope-o"></i>
        <span>Kurs xatlari</span>
    </a>
</li>
