<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{asset('/assets/img/fav.png')}}" >
    <!-- Author Meta -->
    <meta name="author" content="colorlib">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>{{__('words.tashkilot')}}</title>
    <meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
    CSS
    ============================================= -->
    <link rel="stylesheet" href="{{asset('/assets/css/linearicons.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/css/main.css')}}">
</head>
<body>
<header id="header" id="home">
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-sm-6 col-8 header-top-left no-padding">
                    <ul>
                        <li><a href="{{url('/lang/uz')}}">Uz</a></li>
                        <li><a href="{{url('/lang/ru')}}">Ru</a></li>
                        <li><a href="{{url('/lang/en')}}">En</a></li>
                    </ul>
                </div>
                <div class="col-lg-6 col-sm-6 col-4 header-top-right no-padding">
                    <a href="tel:+998 95 621 09 09"><span class="lnr lnr-phone-handset"></span> <span class="text">+998 95 621 09 09</span></a>
                    <a href="/cdn-cgi/l/email-protection#ec9f999c9c839e98ac8f8380839e80858ec28f8381"><span class="lnr lnr-envelope"></span> <span class="text"><span class="__cf_email__" data-cfemail="56252326263924221635393a39243a3f347835393b">info@shchool60.uz</span></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container main-menu">
        <div class="row align-items-center justify-content-between d-flex">
            <div id="logo">
                <a href="{{url('/')}}"><img src="{{asset('/assets/img/logo.png')}}" alt="" title="" /></a>
            </div>
            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li><a href="{{url('/')}}">{{__('words.Bosh-sahifa')}}</a></li>
                    <?php $menus = \App\Models\Menu::query()->whereNull('parent_id')->orderBy('id','asc')->get(); $name = 'name_'.App::getLocale();?>
                    @foreach($menus as $menu)
                        <li><a href="{{url('/page/'.$menu->slug)}}">{{$menu->$name}}</a>
                            <?php $menus1 = \App\Models\Menu::query()->where('parent_id',$menu->id)->orderBy('id','asc')->get();?>
                            @if(count($menus1)>0)
                                <ul>
                                    @foreach($menus1 as $menu1)
                                        <li><a href="{{url('/page/'.$menu1->slug)}}"> {{$menu1->$name}}</a>
                                            <?php $menus2 = \App\Models\Menu::query()->where('parent_id',$menu1->id)->orderBy('id','asc')->get();?>
                                            @if(count($menus2)>0)
                                                <ul>
                                                    @foreach($menus2 as $menu2)
                                                        <li><a href="{{url('/page/'.$menu2->slug)}}"> {{$menu2->$name}}</a></li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>

                    @endforeach
                    <li><a href="{{url('/contact')}}">{{__('words.boglanish')}}</a></li>
                </ul>
            </nav><!-- #nav-menu-container -->
        </div>
    </div>
</header><!-- #header -->
@yield('content')
<!-- Start cta-two Area -->
<section class="cta-two-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 cta-left">
                <h1>{{__('words.20')}}</h1>
            </div>
            <div class="col-lg-4 cta-right">
                <a class="primary-btn wh" href="{{url('/contact')}}">{!! __('words.boglanish') !!}</a>
            </div>
        </div>
    </div>
</section>
<!-- End cta-two Area -->

<!-- start footer Area -->
<footer class="footer-area section-gap">
    <div class="container">
        <div class="row">
            <?php
            $rootmenus = \App\Models\Menu::query()->whereNotNull('parent_id')->distinct('parent_id')->pluck('parent_id');
            $root = \App\Models\Menu::query()->whereIn('id',$rootmenus)->orderBy('id', 'asc')->paginate(4);
            ?>
            @foreach($root as $item)

                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="single-footer-widget">
                        <h4>{{$item->$name}}</h4>
                        <ul>
                            <?php $submenus = \App\Models\Menu::query()->where('parent_id',$item->id)->orderBy('id','asc')->paginate(4);?>
                            @foreach($submenus  as $submenu)
                                <li><a href="{{url('/page')}}">{{$submenu->$name}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endforeach
            <div class="col-lg-4  col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h4>{{__('words.26')}}</h4>
                    <p>{{__('words.27')}}</p>
                    <div class="" id="mc_embed_signup">
                        <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get">
                            <div class="input-group">
                                <input type="text" class="form-control" name="EMAIL" placeholder="E-mail " onfocus="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = ''" onblur="if (!window.__cfRLUnblockHandlers) return false; this.placeholder = 'Enter Email Address '" required="" type="email" data-cf-modified-06539ef40948ba3e23befcac-="">
                                <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <span class="lnr lnr-arrow-right"></span>
                                    </button>
                                </div>
                                <div class="info"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom row align-items-center justify-content-between">
            <p class="footer-text m-0 col-lg-6 col-md-12"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                &copy;<script type="06539ef40948ba3e23befcac-text/javascript">document.write(new Date().getFullYear());</script> {{__('words.24')}} | {{__('words.25')}} <i class="fa fa-heart-o" aria-hidden="true"></i> <a href="http://www.idealsoft.uz/" target="_blank">IdealSoft</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            <div class="col-lg-6 col-sm-12 footer-social">
                <a href="#"><i class="fa fa-2x fa-facebook"></i></a>
                <a href="#"><i class="fa fa-2x fa-telegram"></i></a>
            </div>
        </div>
    </div>
</footer>
<!-- End footer Area -->
<script src="{{asset('/assets/js/vendor/jquery-2.2.4.min.js')}}" type="06539ef40948ba3e23befcac-text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous" type="06539ef40948ba3e23befcac-text/javascript"></script>
<script src="{{asset('/assets/js/vendor/bootstrap.min.js')}}" type="06539ef40948ba3e23befcac-text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA" type="06539ef40948ba3e23befcac-text/javascript"></script>
<script src="{{asset('/assets/js/easing.min.js')}}" type="06539ef40948ba3e23befcac-text/javascript"></script>
<script src="{{asset('/assets/js/hoverIntent.js')}}" type="06539ef40948ba3e23befcac-text/javascript"></script>
<script src="{{asset('/assets/js/superfish.min.js')}}" type="06539ef40948ba3e23befcac-text/javascript"></script>
<script src="{{asset('/assets/js/jquery.ajaxchimp.min.js')}}" type="06539ef40948ba3e23befcac-text/javascript"></script>
<script src="{{asset('/assets/js/jquery.magnific-popup.min.js')}}" type="06539ef40948ba3e23befcac-text/javascript"></script>
<script src="{{asset('/assets/js/jquery.tabs.min.js')}}" type="06539ef40948ba3e23befcac-text/javascript"></script>
<script src="{{asset('/assets/js/jquery.nice-select.min.js')}}" type="06539ef40948ba3e23befcac-text/javascript"></script>
<script src="{{asset('/assets/js/owl.carousel.min.js')}}" type="06539ef40948ba3e23befcac-text/javascript"></script>
<script src="{{asset('/assets/js/mail-script.js')}}" type="06539ef40948ba3e23befcac-text/javascript"></script>
<script src="{{asset('/assets/js/main.js')}}" type="06539ef40948ba3e23befcac-text/javascript"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="06539ef40948ba3e23befcac-text/javascript"></script>
<script type="06539ef40948ba3e23befcac-text/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/a2bd7673/cloudflare-static/rocket-loader.min.js" data-cf-settings="06539ef40948ba3e23befcac-|49" defer=""></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<style>
    .has-error{
        border: 1px solid red!important;
    }
</style>
<script>
    function make_rate(news_id,rate) {
        $.ajax({
            url : "{{url('/rate')}}/"+news_id+"/"+rate,
            method:"get",
            success(data){
                Swal.fire(
                    '{{__('words.Xabar')}}',
                    '{{__('words.ratemsg')}}!',
                    'success'
                )
            },
        });
    }
    function addComment(news_id){
        $('#comment-author').val('');
        $('#comment-email').val('');
        $('#comment-message').val('');

        $("#comment-news-id").val(news_id);
        $("#comment-modal").modal('show');
    }
    function send_comment() {
        error = 0;
        if ($('#comment-author').val()==''){
            $('#comment-author').addClass('has-error');
            error = 1;
        }else{
            $('#comment-author').removeClass('has-error');
        }

        if ($('#comment-email').val()==''){
            $('#comment-email').addClass('has-error');
            error = 1;
        }else{
            $('#comment-email').removeClass('has-error');
        }

        if ($('#comment-message').val()==''){
            $('#comment-message').addClass('has-error');
            error = 1;
        }else{
            $('#comment-message').removeClass('has-error');
        }
        if (error){return;}

        $.ajax({
            url : "{{url('/add-comment')}}",
            method:"post",
            data : {
                "_token": $('#csrf-token')[0].content,
                "name":$('#comment-author').val(),
                "email":$('#comment-email').val(),
                "message":$('#comment-message').val(),
                "news_id":$('#comment-news-id').val(),
            },
            success(data){
                $("#comment-modal").modal('hide');
                comments();
            },
        });
    }
    // window.onload=function () {
    //     SpecialView.run();
    // }

</script>
@yield('scripts')
<div class="modal" tabindex="-1" role="dialog"  id="comment-modal"  style="margin-top: 100px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{__('words.comment')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <lanel>{{__('words.name')}}</lanel>
                <input type="text" id="comment-author" class="form-control">
                <lanel>E-mail</lanel>
                <input type="text" id="comment-email" class="form-control">
                <lanel>{{__('words.comment')}}</lanel>
                <textarea id="comment-message" class="form-control"></textarea>
                <input type="hidden" id="comment-news-id">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">{{__('words.close')}}</button>
                <button type="button" class="btn btn-primary" onclick="send_comment()">{{__('words.send')}}</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>