<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Kurs nomi:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Saqlash', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('courses.index') }}" class="btn btn-danger">Bekor qilish</a>
</div>
