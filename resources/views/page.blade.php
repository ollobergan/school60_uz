@extends('layouts.frontend')

@section('content')
    <!-- page-title -->
    <div class="ttm-page-title-row">
        <div class="section-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-box">
                        <div class="page-title-heading">
                            <?php
                            $lang_title = "title_".App::getLocale();
                            $lang_content = "content_".App::getLocale();

                            ?>
                            <h1 class="title">{{$page->$lang_title}}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- page-title end-->

    <!--site-main start-->
    <div class="site-main">

        <!-- ttm-sidebar -->
        <section class="ttm-sidebar ttm-bgcolor-grey clearfix">
            <div class="container">
                <!-- row -->
                <div class="row ttm-sidebar-left">
                    <div class="col-lg-12 content-area">
                        <div class="row">
                            <div class="col-md-12 pr-5 res-767-plr-15"><img class="img-fluid pb-5" src="images/blog/04.jpg" alt=""></div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="mt-30 mb-35">
                                    {!! $page->$lang_content !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- row end -->
            </div>
        </section>
        <!-- ttm-sidebar end -->

    </div><!--site-main end-->
@endsection
