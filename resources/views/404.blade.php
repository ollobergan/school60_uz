@extends('layouts.frontend')

@section('content')
    <section class="banner-area relative about-banner" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                        {{__('words.21')}}
                    </h1>

                </div>
            </div>
        </div>
    </section>
<section>
    <div class="text-center">
        <img src="{{asset('images/404.jpg')}}" width="400">
        <div style="padding: 100px 0;">
            <h1>                        {{__('words.22')}}
            </h1>
            <a href="{{url('/')}}" class="genric-btn primary" style="margin-top: 50px;">{{__('words.23')}}</a>
        </div>
    </div>
</section>
@endsection
