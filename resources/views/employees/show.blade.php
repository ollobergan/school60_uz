@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('employees.index') }}">Hodimlar</a>
            </li>
            <li class="breadcrumb-item active">Ma'lumot ko'rish</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Ma'lumotlar</strong>
                                  <a href="{{ route('employees.index') }}" class="btn btn-outline-primary">Orqaga</a>
                             </div>
                             <div class="card-body">
                                 @include('employees.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
