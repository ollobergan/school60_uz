<!-- Position Field -->
<div class="form-group col-sm-6">
    {!! Form::label('position', 'Lavohizimi:') !!}
    {!! Form::select('position', ['uqituvchi' => 'O\'qituvchi', 'direktor' => 'Direktor','direktor-urinbosari'=>'Direktor o\'rinbosari','kasaba-uyushmas'=>'Kasaba uyushmas','hodim'=>'Hodim'], null, ['class' => 'form-control']) !!}
</div>

<!-- Fio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fio', 'F.I.O:') !!}
    {!! Form::text('fio', null, ['class' => 'form-control']) !!}
</div>

<!-- Info Uz Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('info_uz', 'Ma\'lumot Uz:') !!}
    {!! Form::textarea('info_uz', null, ['class' => 'form-control']) !!}
</div>

<!-- Info Ru Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('info_ru', 'Ma\'lumot Ru:') !!}
    {!! Form::textarea('info_ru', null, ['class' => 'form-control']) !!}
</div>

<!-- Info En Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('info_en', 'Ma\'lumot En:') !!}
    {!! Form::textarea('info_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-12">
    {!! Form::label('image', 'Rasm:') !!}
    <input id="fieldID2" type="text" name="image" value="" readonly>
    <a href="{{url('/')}}/filemanager/dialog.php?type=2&amp;field_id=fieldID2&amp;relative_url=1" class="btn btn-outline-primary iframe-btn" type="button">Rasm biriktirish</a>
</div>
<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Saqlash', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('employees.index') }}" class="btn btn-outline-danger">Bekor qilish</a>
</div>
