<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $employee->id }}</p>
</div>

<!-- Position Field -->
<div class="form-group">
    {!! Form::label('position', 'Lavozimi:') !!}
    <p>{{ $employee->position }}</p>
</div>

<!-- Fio Field -->
<div class="form-group">
    {!! Form::label('fio', 'F.I.O:') !!}
    <p>{{ $employee->fio }}</p>
</div>

<!-- Info Uz Field -->
<div class="form-group">
    {!! Form::label('info_uz', 'Ma\'lumot Uz:') !!}
    <p>{{ $employee->info_uz }}</p>
</div>

<!-- Info Ru Field -->
<div class="form-group">
    {!! Form::label('info_ru', 'Ma\'lumot Ru:') !!}
    <p>{{ $employee->info_ru }}</p>
</div>

<!-- Info En Field -->
<div class="form-group">
    {!! Form::label('info_en', 'Ma\'lumot En:') !!}
    <p>{{ $employee->info_en }}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Rasm:') !!}
    <p>{{ $employee->image }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Qo\'shilgan vaqto:') !!}
    <p>{{ $employee->created_at }}</p>
</div>

