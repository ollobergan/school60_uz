@extends('layouts.frontend')

@section('content')
    <!-- start banner Area -->
    <section class="banner-area relative" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row fullscreen d-flex align-items-center justify-content-between">
                <div class="banner-content col-lg-9 col-md-12">
                    <h1 class="text-uppercase">
                        {{__('words.1')}}
                    </h1>
                    <p class="pt-10 pb-10">
                        {{__('words.2')}}
                    </p>
                    <a href="{{url('/contact')}}" class="primary-btn text-uppercase">{{__('words.boglanish')}}</a>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->

    <!-- Start feature Area -->
    <section class="feature-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="single-feature">
                        <div class="title">
                            <h4>{{__('words.3')}}</h4>
                        </div>
                        <div class="desc-wrap">
                            <p>
                                {{__('words.4')}}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-feature">
                        <div class="title">
                            <h4>{{__('words.5')}}</h4>
                        </div>
                        <div class="desc-wrap">
                            <p>
                                {{__('words.6')}}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-feature">
                        <div class="title">
                            <h4>{{__('words.7')}}</h4>
                        </div>
                        <div class="desc-wrap">
                            <p>
                                {!! __('words.8') !!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End feature Area -->

    <!-- Start popular-course Area -->
    <section class="popular-course-area section-gap">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="menu-content pb-70 col-lg-8">
                    <div class="title text-center">
                        <h1 class="mb-10">{{__('words.yangiliklar')}}</h1>
                        <p>{{__('words.9')}}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="active-popular-carusel">
                    <?php
                    $news = \App\Models\News::query()->orderBy('id','desc')->paginate(8);
                    $title_lang = "title_".App::getLocale();
                    $short_lang = "short_".App::getLocale();
                    ?>
                    @foreach($news as $new)
                    <div class="single-popular-carusel">
                        <div class="thumb-wrap relative">
                            <div class="thumb relative">
                                <div class="overlay overlay-bg"></div>
                                <img class="img-fluid" src="{{url('/source/'.$new->image)}}" alt="">
                            </div>
                            <div class="meta d-flex justify-content-between">
                                <p><span class="lnr lnr-users"></span>  {{$new->view_cnt}}  <span class="lnr lnr-bubble"></span>{{$new->comment_cnt}}</p>
                                <h4>{{date('d.m.y',strtotime($new->created_at))}}</h4>
                            </div>
                        </div>
                        <div class="details">
                            <a href="#">
                                <h4>
                                    {{$new->$title_lang}}
                                </h4>
                            </a>
                            <p>
                                {!! $new->$short_lang !!}
                            </p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- End popular-course Area -->


    <!-- Start search-course Area -->
    <section class="search-course-area relative">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-6 col-md-6 search-course-left">
                    <h1 class="text-white">
                        {!! __('words.10') !!}
                    </h1>
                    <p>
                        {!! __('words.11') !!}
                    </p>
                    <div class="row details-content">
                        <div class="col single-detials">
                            <span class="lnr lnr-graduation-hat"></span>
                            <a href="#"><h4>{!! __('words.12') !!}</h4></a>
                            <p>
                                {!! __('words.13') !!}
                            </p>
                        </div>
                        <div class="col single-detials">
                            <span class="lnr lnr-license"></span>
                            <a href="#"><h4>{!! __('words.14') !!}</h4></a>
                            <p>
                                {!! __('words.15') !!}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 search-course-right section-gap">
                    <form class="form-wrap" action="#">
                        <h4 class="text-white pb-20 text-center mb-30">{!! __('words.16') !!}</h4>
                        <input type="text" class="form-control" name="name" placeholder="{!! __('words.ism') !!}" id="name">
                        <input type="phone" class="form-control" name="phone" placeholder="{!! __('words.phone') !!}" id="phone">
                        <input type="email" class="form-control" name="email" placeholder="Email" id="email">
                        <div class="form-select">
                            <select class="form-control" style="padding: 8px 10px;" id="course-select">
                                <option datd-display="">{!! __('words.17') !!}</option>
                                <?php $courses = \App\Models\Course::query()->orderBy('id','asc')->get();?>
                                @foreach($courses as $course)
                                <option value="{{$course->id}}">{{$course->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <button class="primary-btn text-uppercase" type="button" onclick="send_course_message()">{{__('words.yuborish')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- End search-course Area -->
    <br>
    <section class="info-area pb-120">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-lg-6 no-padding info-area-left">
                    <img class="img-fluid" src="assets/img/about-img.jpg" alt="">
                </div>
                <div class="col-lg-6 info-area-right">
                    <h1>{!! __('words.18') !!}</h1>
                    <p>
                        {!! __('words.19') !!}
                    </p>
                    </div>
            </div>
        </div>
    </section>

@endsection
@section('scripts')
    <script>
        function send_course_message(){
            error = 0;
            if ($('#name').val()==''){
                $('#name').addClass('has-error');
                error = 1;
            }else{
                $('#name').removeClass('has-error');
            }

            if ($('#phone').val()==''){
                $('#phone').addClass('has-error');
                error = 1;
            }else{
                $('#phone').removeClass('has-error');
            }

            if ($('#email').val()==''){
                $('#email').addClass('has-error');
                error = 1;
            }else{
                $('#email').removeClass('has-error');
            }
            if ($('#course-select').val()==''){
                $('#course-select').addClass('has-error');
                error = 1;
            }else{
                $('#course-select').removeClass('has-error');
            }
            console.log(1);
            if (error){return;}
            console.log(2);
            $.ajax({
                url : "{{url('/add-course-message')}}",
                method:"post",
                data : {
                    "_token": $('#csrf-token')[0].content,
                    "name":$('#name').val(),
                    "email":$('#email').val(),
                    "phone":$('#phone').val(),
                    "course_id":$('#course-select').val(),
                },
                success(data){
                    $('#name').val('');
                    $('#email').val('');
                    $('#phone').val('');
                    $('#course-select').val('');
                    Swal.fire(
                        '{{__('words.Xabar')}}',
                        '{{__('words.Xabar-muvofaqqiyatli-yuborildi')}}!',
                        'success'
                    )
                },
            });
        }
    </script>
@endsection