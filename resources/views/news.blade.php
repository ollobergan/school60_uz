@extends('layouts.frontend')

@section('content')
    <?php
    $title_lang = "title_".App::getLocale();
    $short_lang = "short_".App::getLocale();
    ?>
    <section class="banner-area relative about-banner" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center">
                <div class="about-content col-lg-12">
                    <h1 class="text-white">
                        {{__('words.yangiliklar')}}
                    </h1>
                    <p class="text-white link-nav"><a href="{{url('/')}}">{{__('words.Bosh-sahifa')}} </a>  <span class="lnr lnr-arrow-right"></span>  <a href="#">{{__('words.yangiliklar')}}</a></p>
                </div>
            </div>
        </div>
    </section>
    <section class="popular-courses-area section-gap courses-page">
        <div class="container">
            {{--<div class="row d-flex justify-content-center">--}}
            {{--<div class="menu-content pb-70 col-lg-8">--}}
            {{--<div class="title text-center">--}}
            {{--<h1 class="mb-10">Maktabimiz so'nggi yangiliklari</h1>--}}
            {{--<p>There is a moment in the life of any aspiring.</p>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            <div class="row">
                @forelse($news as $item)
                    <div class="single-popular-carusel col-lg-3 col-md-6">
                        <div class="thumb-wrap relative">
                            <div class="thumb relative">
                                <div class="overlay overlay-bg"></div>
                                <img class="img-fluid" src="{{url('/source/'.$item->image)}}" alt="">
                            </div>
                            <div class="meta d-flex justify-content-between">
                                <p><span class="lnr lnr-users"></span> {{$item->view_cnt}} <span class="lnr lnr-bubble"></span>{{$item->comment_cnt}}</p>
                                <h4>{{date('d.m.y',strtotime($item->created_at))}}</h4>
                            </div>
                        </div>
                        <div class="details">
                            <a href="{{url('news/'.$item->id)}}">
                                <h4>
                                    {{$item->$title_lang}}
                                </h4>
                            </a>
                            <p>
                                {!! $item->$short_lang !!}
                            </p>
                        </div>
                    </div>
                @empty
                    <div style="padding: 200px 0;" class="text-center">
                        <h1>{{__('words.yangiliklar-topilmadi')}}</h1>
                    </div>
                @endforelse
            </div>
        </div>
    </section>
@endsection
