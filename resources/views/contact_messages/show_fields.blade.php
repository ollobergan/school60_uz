<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $contactMessage->id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Ismi:') !!}
    <p>{{ $contactMessage->name }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $contactMessage->email }}</p>
</div>

<!-- Message Field -->
<div class="form-group">
    {!! Form::label('message', 'Xabar:') !!}
    <p>{{ $contactMessage->message }}</p>
</div>

<!-- Read Field -->
<div class="form-group">
    {!! Form::label('read', 'O\'qilganligi:') !!}
    <p>{{ $contactMessage->read }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Xabar yaratilgan vaqt:') !!}
    <p>{{ $contactMessage->created_at }}</p>
</div>
