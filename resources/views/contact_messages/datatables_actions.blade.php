{!! Form::open(['route' => ['contactMessages.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('contactMessages.show', $id) }}" class='btn btn-outline-success'>
       <i class="fa fa-eye"></i>
    </a>
    <a href="{{ route('contactMessages.edit', $id) }}" class='btn btn-outline-info'>
       <i class="fa fa-bookmark"></i>
    </a>
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-outline-danger',
        'onclick' => "return confirm('Ishinchinhiz komilmi?')"
    ]) !!}
</div>
{!! Form::close() !!}
