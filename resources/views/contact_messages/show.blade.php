@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('contactMessages.index') }}">Xabarlar</a>
            </li>
            <li class="breadcrumb-item active">Ko'rish</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Xabar</strong>
                                  <a href="{{ route('contactMessages.index') }}" class="btn btn-outline-primary">Orqaga</a>
                             </div>
                             <div class="card-body">
                                 @include('contact_messages.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
