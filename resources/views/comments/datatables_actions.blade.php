{!! Form::open(['route' => ['comments.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('comments.show', $id) }}" class='btn btn-outline-success'>
       <i class="fa fa-eye"></i>
    </a>
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-outline-danger',
        'onclick' => "return confirm('Ishonchingiz komilmi?')"
    ]) !!}
</div>
{!! Form::close() !!}
