@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('comments.index') }}">Komentariyalar</a>
            </li>
            <li class="breadcrumb-item active">Ko'rish</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Ko'rish</strong>
                                  <a href="{{ route('comments.index') }}" class="btn btn-outline-primary">Orqaga</a>
                             </div>
                             <div class="card-body">
                                 @include('comments.show_fields')
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
