<!-- News Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('news_id', 'News Id:') !!}
    {!! Form::text('news_id', null, ['class' => 'form-control']) !!}
</div>

<!-- User Ip Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_ip', 'User Ip:') !!}
    {!! Form::text('user_ip', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Message Field -->
<div class="form-group col-sm-6">
    {!! Form::label('message', 'Message:') !!}
    {!! Form::text('message', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('comments.index') }}" class="btn btn-secondary">Cancel</a>
</div>
