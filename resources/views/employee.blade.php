@extends('layouts.frontend')

@section('content')
    <div class="breadcrumb-banner-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-text">
                        <h1 class="text-center">{{__('words.'.$position.'lar')}}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="event-details-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $info = 'info_'.App::getLocale();
                    ?>
                    @forelse($employees as $item)
                        <div class="col-md-6">
                            <div class="single-latest-item">
                                <div class="single-latest-image">
                                    <img src="{{url('/source/'.$item->image)}}" style="height: 225px" alt="">
                                </div>
                                <div class="single-latest-text" style="height: 225px">
                                    <h3><a href="#">{{__('words.'.$item->position)}}</a></h3>

                                    <p>{!! $item->$info !!}</p>
                                </div>
                            </div>
                        </div>
                    @empty
                        <div style="padding: 200px 0;">
                            <h1>{{__('words.Ma-lumot-topilmadi')}}</h1>
                        </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
    <!--End of Event Details Area-->
@endsection
