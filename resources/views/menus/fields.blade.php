<!-- Parent Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('parent_id', 'Qism menyu:') !!}
    {!! Form::select('parent_id', $parents,null, ['class' => 'form-control']) !!}
</div>

<!-- Name Uz Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name_uz', 'Nomi Uz:') !!}
    {!! Form::text('name_uz', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Ru Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name_ru', 'Nomi Ru:') !!}
    {!! Form::text('name_ru', null, ['class' => 'form-control']) !!}
</div>

<!-- Name En Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name_en', 'Nomi En:') !!}
    {!! Form::text('name_en', null, ['class' => 'form-control']) !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Saqlash', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('menus.index') }}" class="btn btn-danger">Bekor qilish</a>
</div>
