@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('menus.index') !!}">Menyular</a>
      </li>
      <li class="breadcrumb-item active">Qo'shish</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                @include('coreui-templates::common.errors')
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Menyu qo'shish</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'menus.store']) !!}

                                   @include('menus.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
