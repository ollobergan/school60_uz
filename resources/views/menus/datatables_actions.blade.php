{!! Form::open(['route' => ['menus.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('menus.edit', $id) }}" class='btn btn-outline-info'>
       <i class="fa fa-edit"></i>
    </a>
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-outline-danger',
        'onclick' => "return confirm('Ishonchingiz komilmi?')"
    ]) !!}
</div>
{!! Form::close() !!}
