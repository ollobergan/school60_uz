<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/contact', 'HomeController@contact');
Route::post('/contact', 'HomeController@contact_save');

Route::get('/page/{slug}', 'PageController@get_page');
Route::get('/news/', 'NewsController@get_news_list');
Route::get('/news/{id}', 'NewsController@get_post');
Route::get('lang/{locale}', 'LanguageController@index');
Route::get('/employee/{slug}', 'EmployeeController@get_employees');
Route::get('/rate/{news_id}/{rate}', 'RateController@make_rate');
Route::get('/get-news-comments/{news_id}', 'CommentController@get_news_comments');
Route::post('/add-comment', 'CommentController@add_comment');
Route::post('/add-course-message', 'Course_messageController@add_message');


//
Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder')->name('io_generator_builder');

Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate')->name('io_field_template');

Route::get('relation_field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@relationFieldTemplate')->name('io_relation_field_template');

Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate')->name('io_generator_builder_generate');

Route::post('generator_builder/rollback', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@rollback')->name('io_generator_builder_rollback');

Route::post(
    'generator_builder/generate-from-file',
    '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generateFromFile'
)->name('io_generator_builder_generate_from_file');


Auth::routes(['register' => false]);
Route::group(['prefix'=>'admin','middleware'=>'auth'], function() {
    Route::get('/home', 'HomeController@home');
    Route::resource('users', 'UserController');
    Route::resource('news', 'NewsController');
    Route::resource('pages', 'PageController');
    Route::resource('employees', 'EmployeeController');
    Route::resource('contactMessages', 'Contact_messageController');
});

Route::resource('comments', 'CommentController');

Route::resource('menus', 'MenuController');

Route::resource('courses', 'CourseController');

Route::resource('courseMessages', 'Course_messageController');