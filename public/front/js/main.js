(function ($) {
    "use strict";
    $(".carousel-inner .item:first-child").addClass("active");
    /* Mobile menu click then remove
    ==========================*/
    $(".mainmenu-area #mainmenu li a").on("click", function () {
        $(".navbar-collapse").removeClass("in");
    });
    /* Scroll to top
    ===================*/
    $.scrollUp({
        scrollText: '<i class="zmdi zmdi-long-arrow-up"></i>',
        easingType: 'linear',
        scrollSpeed: 900,
        animation: 'fade'
    });
    $('.gallery').owlCarousel({
        loop: true,
        margin: 0,
        responsiveClass: true,
        nav: true,
        autoplay: true,
        autoplayTimeout: 4000,
        smartSpeed: 1000,
        navText: ['<img src="images/arrow-right.png" alt="">', '<img src="images/arrow-left.png" alt="">'],
        responsive: {
            0: {
                items: 1,
            },
            480: {
                items: 2
            },
            600: {
                items: 3
            },
            1200: {
                items: 4
            },
            1500: {
                items: 6
            }
        }
    });


    /*--------------------
       MAGNIFIC POPUP JS
       ----------------------*/

    /*-----------------
    Mesonary jQuery
    -------------------*/
    var $boxes = $('.ms-item');
    $boxes.hide();
    var $container = $('.ms-items');
    $container.imagesLoaded(function () {
        $boxes.fadeIn();
        $container.masonry({
            itemSelector: '.ms-item',
        });
    });
    /* Preloader Js
    ===================*/
    $(window).on("load", function () {
        /*WoW js Active
        =================*/
        new WOW().init({
            mobile: false,
        });
    });
})(jQuery);