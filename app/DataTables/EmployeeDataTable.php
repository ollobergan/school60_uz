<?php

namespace App\DataTables;

use App\Models\Employee;
use Illuminate\Support\Facades\URL;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class EmployeeDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'employees.datatables_actions')
            ->editColumn('image',function ($data){
                return "<img src='".URL::to('/source/'.$data->image)."' width=100>";
            })->rawColumns(['action','image']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Employee $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Employee $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->language(["url"=>"//cdn.datatables.net/plug-ins/1.10.20/i18n/Uzbek.json"])
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-outline-primary btn-sm no-corner','text'=>"<i class='fa fa-plus'></i> Qo'shish"],
                    ['extend' => 'excel', 'className' => 'btn btn-outline-primary btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-outline-primary btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-outline-primary btn-sm no-corner','text'=>"<i class=\"fa fa-refresh\"></i> Yangilash"],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'image'=>['title'=>'Rasm'],
            'fio'=>['title'=>'F.I.O'],
            'position'=>['title'=>"Lavozimi"]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'employeesdatatable_' . time();
    }
}
