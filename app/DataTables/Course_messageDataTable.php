<?php

namespace App\DataTables;

use App\Models\Course;
use App\Models\Course_message;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class Course_messageDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'course_messages.datatables_actions')
            ->editColumn('course_id',function ($data){
                $course = Course::find($data->course_id);
                return $course->name;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Course_message $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Course_message $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->language(["url"=>"//cdn.datatables.net/plug-ins/1.10.20/i18n/Uzbek.json"])
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'excel', 'className' => 'btn btn-outline-primary btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-outline-primary btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-outline-primary btn-sm no-corner','text'=>"<i class=\"fa fa-refresh\"></i> Yangilash"],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'course_id'=>['title'=>'Kurs'],
            'name'=>['title'=>'Ismi']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'course_messagesdatatable_' . time();
    }
}
