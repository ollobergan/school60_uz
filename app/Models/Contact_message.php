<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Contact_message
 * @package App\Models
 * @version February 20, 2020, 8:00 pm UTC
 *
 * @property string name
 * @property string email
 * @property string message
 * @property string read
 */
class Contact_message extends Model
{

    public $table = 'contact_messages';
    



    public $fillable = [
        'name',
        'email',
        'message',
        'read'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'message' => 'string',
        'read' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
