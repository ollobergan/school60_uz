<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Page
 * @package App\Models
 */
class Rate extends Model
{

    public $table = 'rates';

    public $fillable = [
        'news_id',
        'user_ip',
        'rate',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'news_id' => 'integer',
        'user_ip' => 'string',
        'rate' => 'float',
    ];


    
}
