<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Employee
 * @package App\Models
 * @version February 20, 2020, 7:56 pm UTC
 *
 * @property string position
 * @property integer fio
 * @property string info_uz
 * @property string info_ru
 * @property string info_en
 * @property string image
 */
class Employee extends Model
{

    public $table = 'employees';
    



    public $fillable = [
        'position',
        'fio',
        'info_uz',
        'info_ru',
        'info_en',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'position' => 'string',
        'fio' => 'integer',
        'info_uz' => 'string',
        'info_ru' => 'string',
        'info_en' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'position' => 'required',
        'fio' => 'required|unique:employees',
        'info_uz' => 'required'
    ];

    
}
