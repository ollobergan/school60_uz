<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class News
 * @package App\Models
 * @version February 20, 2020, 7:41 pm UTC
 *
 * @property string title_uz
 * @property string title_ru
 * @property string title_en
 * @property string short_uz
 * @property string short_ru
 * @property string short_en
 * @property string content_uz
 * @property string content_ru
 * @property string content_en
 * @property string image
 */
class News extends Model
{

    public $table = 'news';
    



    public $fillable = [
        'title_uz',
        'title_ru',
        'title_en',
        'short_uz',
        'short_ru',
        'short_en',
        'content_uz',
        'content_ru',
        'content_en',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title_uz' => 'string',
        'title_ru' => 'string',
        'title_en' => 'string',
        'short_uz' => 'string',
        'short_ru' => 'string',
        'short_en' => 'string',
        'content_uz' => 'string',
        'content_ru' => 'string',
        'content_en' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title_uz' => 'required',
        'short_uz' => 'required',
        'content_uz' => 'required'
    ];

    
}
