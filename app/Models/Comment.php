<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Comment
 * @package App\Models
 * @version February 23, 2020, 3:10 pm UTC
 *
 * @property integer news_id
 * @property string user_ip
 * @property string name
 * @property string email
 * @property string message
 */
class Comment extends Model
{

    public $table = 'comments';
    



    public $fillable = [
        'news_id',
        'user_ip',
        'name',
        'email',
        'message'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'news_id' => 'integer',
        'user_ip' => 'string',
        'name' => 'string',
        'email' => 'string',
        'message' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
