<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Course_message
 * @package App\Models
 * @version February 27, 2020, 10:25 am UTC
 *
 * @property integer course_id
 * @property string name
 * @property string email
 * @property string phone
 */
class Course_message extends Model
{

    public $table = 'course_messages';
    



    public $fillable = [
        'course_id',
        'name',
        'email',
        'phone'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'course_id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'phone' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
