<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Menu
 * @package App\Models
 * @version February 25, 2020, 1:03 am UTC
 *
 * @property integer parent_id
 * @property string name_uz
 * @property string name_ru
 * @property string name_en
 * @property string slug
 */
class Menu extends Model
{

    public $table = 'menus';
    



    public $fillable = [
        'parent_id',
        'name_uz',
        'name_ru',
        'name_en',
        'slug'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'parent_id' => 'integer',
        'name_uz' => 'string',
        'name_ru' => 'string',
        'name_en' => 'string',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name_uz' => 'required',
        'name_ru' => 'required',
        'name_en' => 'required',
    ];

    
}
