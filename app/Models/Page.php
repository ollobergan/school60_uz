<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Page
 * @package App\Models
 * @version February 20, 2020, 7:51 pm UTC
 *
 * @property string slug
 * @property string title_uz
 * @property string title_ru
 * @property string title_en
 * @property string content_uz
 * @property string content_ru
 * @property string content_en
 */
class Page extends Model
{

    public $table = 'pages';
    



    public $fillable = [
        'slug',
        'title_uz',
        'title_ru',
        'title_en',
        'content_uz',
        'content_ru',
        'content_en'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'slug' => 'string',
        'title_uz' => 'string',
        'title_ru' => 'string',
        'title_en' => 'string',
        'content_uz' => 'string',
        'content_ru' => 'string',
        'content_en' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'slug' => 'required',
        'title_uz' => 'required',
        'content_uz' => 'required'
    ];

    
}
