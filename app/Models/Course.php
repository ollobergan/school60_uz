<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Course
 * @package App\Models
 * @version February 27, 2020, 10:19 am UTC
 *
 * @property string name
 */
class Course extends Model
{

    public $table = 'courses';
    



    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
