<?php

namespace App\Repositories;

use App\Models\Contact_message;
use App\Repositories\BaseRepository;

/**
 * Class Contact_messageRepository
 * @package App\Repositories
 * @version February 20, 2020, 8:00 pm UTC
*/

class Contact_messageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'message',
        'read'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Contact_message::class;
    }
}
