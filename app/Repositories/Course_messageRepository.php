<?php

namespace App\Repositories;

use App\Models\Course_message;
use App\Repositories\BaseRepository;

/**
 * Class Course_messageRepository
 * @package App\Repositories
 * @version February 27, 2020, 10:25 am UTC
*/

class Course_messageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'course_id',
        'name',
        'email',
        'phone'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Course_message::class;
    }
}
