<?php

namespace App\Http\Controllers;

use App\Models\Contact_message;
use App\Models\News;
use App\Models\Page;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $news = News::query()->orderBy('created_at','desc')->limit(3)->get();
        $news2 = News::query()->orderBy('created_at','desc')->offset(3)->limit(4)->get();
        return view('index',compact('news','news2'));
    }
    public function home()
    {
        $data = new \stdClass();
        $data->news_cnt = News::query()->count();
        $data->pages_cnt = Page::query()->count();
        $data->admin_cnt = User::query()->count();
        $data->messages_cnt = Contact_message::query()->count();
        return view('home',compact('data'));
    }

    public function contact()
    {
        return view('contact');
    }

    public function contact_save(Request $request)
    {
        $msg = new Contact_message();
        $msg->name = $request->name;
        $msg->email = $request->email;
        $msg->message = $request->message;
        $msg->save();
        echo "success";
    }
}
