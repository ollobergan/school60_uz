<?php

namespace App\Http\Controllers;

use App\DataTables\Course_messageDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCourse_messageRequest;
use App\Http\Requests\UpdateCourse_messageRequest;
use App\Models\Course_message;
use App\Repositories\Course_messageRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;

class Course_messageController extends AppBaseController
{
    /** @var  Course_messageRepository */
    private $courseMessageRepository;

    public function __construct(Course_messageRepository $courseMessageRepo)
    {
        $this->courseMessageRepository = $courseMessageRepo;
    }

    /**
     * Display a listing of the Course_message.
     *
     * @param Course_messageDataTable $courseMessageDataTable
     * @return Response
     */
    public function index(Course_messageDataTable $courseMessageDataTable)
    {
        return $courseMessageDataTable->render('course_messages.index');
    }

    /**
     * Show the form for creating a new Course_message.
     *
     * @return Response
     */
    public function create()
    {
        return view('course_messages.create');
    }

    /**
     * Store a newly created Course_message in storage.
     *
     * @param CreateCourse_messageRequest $request
     *
     * @return Response
     */
    public function store(CreateCourse_messageRequest $request)
    {
        $input = $request->all();

        $courseMessage = $this->courseMessageRepository->create($input);

        Flash::success('Course Message saved successfully.');

        return redirect(route('courseMessages.index'));
    }

    /**
     * Display the specified Course_message.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $courseMessage = $this->courseMessageRepository->find($id);

        if (empty($courseMessage)) {
            Flash::error('Course Message not found');

            return redirect(route('courseMessages.index'));
        }

        return view('course_messages.show')->with('courseMessage', $courseMessage);
    }

    /**
     * Show the form for editing the specified Course_message.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $courseMessage = $this->courseMessageRepository->find($id);

        if (empty($courseMessage)) {
            Flash::error('Course Message not found');

            return redirect(route('courseMessages.index'));
        }

        return view('course_messages.edit')->with('courseMessage', $courseMessage);
    }

    /**
     * Update the specified Course_message in storage.
     *
     * @param  int              $id
     * @param UpdateCourse_messageRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCourse_messageRequest $request)
    {
        $courseMessage = $this->courseMessageRepository->find($id);

        if (empty($courseMessage)) {
            Flash::error('Course Message not found');

            return redirect(route('courseMessages.index'));
        }

        $courseMessage = $this->courseMessageRepository->update($request->all(), $id);

        Flash::success('Course Message updated successfully.');

        return redirect(route('courseMessages.index'));
    }

    /**
     * Remove the specified Course_message from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $courseMessage = $this->courseMessageRepository->find($id);

        if (empty($courseMessage)) {
            Flash::error('Course Message not found');

            return redirect(route('courseMessages.index'));
        }

        $this->courseMessageRepository->delete($id);

        Flash::success('Course Message deleted successfully.');

        return redirect(route('courseMessages.index'));
    }

    public function add_message(Request $request)
    {
        $newmsg = new Course_message();
        $newmsg->fill($request->all());
        $newmsg->save();
    }
}
