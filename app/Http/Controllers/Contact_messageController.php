<?php

namespace App\Http\Controllers;

use App\DataTables\Contact_messageDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateContact_messageRequest;
use App\Http\Requests\UpdateContact_messageRequest;
use App\Models\Contact_message;
use App\Repositories\Contact_messageRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class Contact_messageController extends AppBaseController
{
    /** @var  Contact_messageRepository */
    private $contactMessageRepository;

    public function __construct(Contact_messageRepository $contactMessageRepo)
    {
        $this->contactMessageRepository = $contactMessageRepo;
    }

    /**
     * Display a listing of the Contact_message.
     *
     * @param Contact_messageDataTable $contactMessageDataTable
     * @return Response
     */
    public function index(Contact_messageDataTable $contactMessageDataTable)
    {
        return $contactMessageDataTable->render('contact_messages.index');
    }

    /**
     * Show the form for creating a new Contact_message.
     *
     * @return Response
     */
    public function create()
    {
        return view('contact_messages.create');
    }

    /**
     * Store a newly created Contact_message in storage.
     *
     * @param CreateContact_messageRequest $request
     *
     * @return Response
     */
    public function store(CreateContact_messageRequest $request)
    {
        $input = $request->all();

        $contactMessage = $this->contactMessageRepository->create($input);

        echo "success";
    }

    /**
     * Display the specified Contact_message.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $contactMessage = $this->contactMessageRepository->find($id);

        if (empty($contactMessage)) {
            Flash::error('Contact Message not found');

            return redirect(route('contactMessages.index'));
        }

        return view('contact_messages.show')->with('contactMessage', $contactMessage);
    }

    /**
     * Show the form for editing the specified Contact_message.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $message = Contact_message::find($id);
        $message->read = "O'qilgan";
        $message->save();

        Flash::success('Xabar o\'qilgan deb belgilandi.');

        return redirect(route('contactMessages.index'));
    }


    /**
     * Remove the specified Contact_message from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $contactMessage = $this->contactMessageRepository->find($id);

        if (empty($contactMessage)) {
            Flash::error('Contact Message not found');

            return redirect(route('contactMessages.index'));
        }

        $this->contactMessageRepository->delete($id);

        Flash::success('Xabar o\'chirildi.');

        return redirect(route('contactMessages.index'));
    }
}
