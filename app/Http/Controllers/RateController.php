<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\Rate;
use Illuminate\Http\Request;

class RateController extends Controller
{
    public function make_rate($news_id, $rate,Request $request)
    {
        $user_ip = $request->getClientIp();
        $rate = Rate::query()->where('user_ip','like',$user_ip)->where('news_id',$news_id)->first();
        if (count($rate)==0)
        {
            $rate = new Rate();
            $rate->user_ip = $user_ip;
            $rate->rate = $rate;
            $rate->news_id = $news_id;
            $rate->save();
        }else{
            $rate->rate = $rate;
            $rate->save();
        }
        $summ_rates = Rate::query()->where('news_id',$news_id)->summ('rate');
        $count_rates = Rate::query()->where('news_id',$news_id)->count();
        $news = News::find($news_id);
        $news->rate = $summ_rates/$count_rates;
        $news->rate_cnt = $count_rates;
        $news->save();
        return "success";
    }
}
